FROM almalinux/9-minimal

RUN /bin/microdnf -y install crypto-policies-scripts openssh-clients sshpass && \
    /bin/microdnf -y clean all && \
    /bin/update-crypto-policies --set DEFAULT:SHA1

COPY 10-legacy-crypto.conf /etc/ssh/ssh_config.d/

ENTRYPOINT ["/bin/ssh"]
CMD ["--help"]
